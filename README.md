# pihat-project

# Introduction

The use case of this PiHat is to monitor, regulate and control the temperature of an attached
external device which contains a heating element. It uses a switching voltage regulator
subsystem to power the Raspberry Pi and LED and Temperature sensing subsystems.

# Usage instruction:

The system allows a user to connect the system to a heater power cable(via the onboard
relay) for example and allows the user to specify an exact temperature which allows
extremely customised temperature control in contrast to adjusting the temperature with a
control knob as with most generic heaters. This allows for a room temperature to be decently
maintained.

# usage examples:
### ex.1 - [HOUSE HEATING SYSTEM]
This system can be attached to a house heating system in order to regulate the temperature
through the entire house according to user desires. They will be able to wirelessly monitor
the status of the home temperature.

### ex.2 - [GEYSER HEAT CONTROL]
Homeowners want a way to control their hot water needs by reducing electricity use and
therefore saving money. This is where the temperature controller module comes into play. It
will connect to a geyser control unit.

# Contributing

## Bugs and feature requests
If you've noticed a bug or have a feature request, then you should make a new branch. 

## Fork & create a branch
If this is something you think you can fix, then fork master and create a branch with a descriptive name.
A good branch name would be (where issue #325 is the ticket you're working on):

