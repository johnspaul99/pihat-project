EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R2
U 1 1 60BBE4C5
P 3750 3150
F 0 "R2" H 3820 3196 50  0000 L CNN
F 1 "330" H 3820 3105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3680 3150 50  0001 C CNN
F 3 "~" H 3750 3150 50  0001 C CNN
	1    3750 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 60BBE4CB
P 3750 3650
F 0 "D1" V 3789 3532 50  0000 R CNN
F 1 "LED" V 3698 3532 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 3750 3650 50  0001 C CNN
F 3 "~" H 3750 3650 50  0001 C CNN
	1    3750 3650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3750 3300 3750 3500
$Comp
L Transistor_BJT:2N3904 Q1
U 1 1 60BBE4D2
P 3650 4200
F 0 "Q1" H 3840 4246 50  0000 L CNN
F 1 "2N3904" H 3840 4155 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 3850 4125 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/2N3903-D.PDF" H 3650 4200 50  0001 L CNN
	1    3650 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 3800 3750 4000
$Comp
L pspice:0 #GND01
U 1 1 60BBE4D9
P 3750 4850
F 0 "#GND01" H 3750 4750 50  0001 C CNN
F 1 "0" H 3750 4939 50  0000 C CNN
F 2 "" H 3750 4850 50  0001 C CNN
F 3 "~" H 3750 4850 50  0001 C CNN
	1    3750 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 4400 3750 4850
Wire Wire Line
	3750 3000 3750 2750
$Comp
L Device:R R1
U 1 1 60BBEC3F
P 3150 4200
F 0 "R1" V 2943 4200 50  0000 C CNN
F 1 "3000" V 3034 4200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3080 4200 50  0001 C CNN
F 3 "~" H 3150 4200 50  0001 C CNN
	1    3150 4200
	0    1    1    0   
$EndComp
Text GLabel 2850 4200 0    50   Input ~ 0
GPIO17
Wire Wire Line
	3000 4200 2850 4200
Wire Wire Line
	3300 4200 3450 4200
$Comp
L Device:R R4
U 1 1 60BD82F0
P 5550 3150
F 0 "R4" H 5620 3196 50  0000 L CNN
F 1 "330" H 5620 3105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5480 3150 50  0001 C CNN
F 3 "~" H 5550 3150 50  0001 C CNN
	1    5550 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 60BD82F6
P 5550 3650
F 0 "D2" V 5589 3532 50  0000 R CNN
F 1 "LED" V 5498 3532 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 5550 3650 50  0001 C CNN
F 3 "~" H 5550 3650 50  0001 C CNN
	1    5550 3650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 3300 5550 3500
$Comp
L Transistor_BJT:2N3904 Q2
U 1 1 60BD82FD
P 5450 4200
F 0 "Q2" H 5640 4246 50  0000 L CNN
F 1 "2N3904" H 5640 4155 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 5650 4125 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/2N3903-D.PDF" H 5450 4200 50  0001 L CNN
	1    5450 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3800 5550 4000
Wire Wire Line
	5550 3000 5550 2750
$Comp
L Device:R R3
U 1 1 60BD830C
P 4950 4200
F 0 "R3" V 4743 4200 50  0000 C CNN
F 1 "3000" V 4834 4200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4880 4200 50  0001 C CNN
F 3 "~" H 4950 4200 50  0001 C CNN
	1    4950 4200
	0    1    1    0   
$EndComp
Text GLabel 4650 4200 0    50   Input ~ 0
GPIO27
Wire Wire Line
	4800 4200 4650 4200
Wire Wire Line
	5100 4200 5250 4200
$Comp
L Device:R R6
U 1 1 60BDE488
P 7250 3150
F 0 "R6" H 7320 3196 50  0000 L CNN
F 1 "330" H 7320 3105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 7180 3150 50  0001 C CNN
F 3 "~" H 7250 3150 50  0001 C CNN
	1    7250 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 60BDE48E
P 7250 3650
F 0 "D3" V 7289 3532 50  0000 R CNN
F 1 "LED" V 7198 3532 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 7250 3650 50  0001 C CNN
F 3 "~" H 7250 3650 50  0001 C CNN
	1    7250 3650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7250 3300 7250 3500
$Comp
L Transistor_BJT:2N3904 Q3
U 1 1 60BDE495
P 7150 4200
F 0 "Q3" H 7340 4246 50  0000 L CNN
F 1 "2N3904" H 7340 4155 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 7350 4125 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/2N3903-D.PDF" H 7150 4200 50  0001 L CNN
	1    7150 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 3800 7250 4000
$Comp
L pspice:0 #GND03
U 1 1 60BDE49C
P 7250 4850
F 0 "#GND03" H 7250 4750 50  0001 C CNN
F 1 "0" H 7250 4939 50  0000 C CNN
F 2 "" H 7250 4850 50  0001 C CNN
F 3 "~" H 7250 4850 50  0001 C CNN
	1    7250 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 4400 7250 4850
Wire Wire Line
	7250 3000 7250 2750
$Comp
L Device:R R5
U 1 1 60BDE4A4
P 6650 4200
F 0 "R5" V 6443 4200 50  0000 C CNN
F 1 "3000" V 6534 4200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 6580 4200 50  0001 C CNN
F 3 "~" H 6650 4200 50  0001 C CNN
	1    6650 4200
	0    1    1    0   
$EndComp
Text GLabel 6350 4200 0    50   Input ~ 0
GPIO22
Wire Wire Line
	6500 4200 6350 4200
Wire Wire Line
	6800 4200 6950 4200
Connection ~ 5550 2750
Wire Wire Line
	5550 2750 7250 2750
Wire Wire Line
	7250 2750 8300 2750
Wire Wire Line
	8300 2750 8300 3650
Connection ~ 7250 2750
Text GLabel 8300 3650 3    50   Input ~ 0
3.3V
Wire Wire Line
	3750 2750 5550 2750
Text Notes 7750 6950 0    118  ~ 0
STATUS LED SUBSYSTEM
Text Notes 8150 7650 0    59   ~ 0
04/06/2021
Text Notes 10600 7650 0    59   ~ 0
1.00
$Comp
L pspice:0 #GND0101
U 1 1 60BF0533
P 5550 4850
F 0 "#GND0101" H 5550 4750 50  0001 C CNN
F 1 "0" H 5550 4939 50  0000 C CNN
F 2 "" H 5550 4850 50  0001 C CNN
F 3 "~" H 5550 4850 50  0001 C CNN
	1    5550 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 4850 5550 4400
$EndSCHEMATC
