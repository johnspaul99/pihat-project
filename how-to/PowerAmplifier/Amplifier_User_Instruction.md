<h3>Amplifier User Instruction</h3>

The use case of this Amplifier is to monitor, regulate and control the temperature of an attached external device which contains a heating element.

It uses a switching voltage regulator subsystem to provide a supply voltage of 3.3V

The amplifier circuit allows a user to connect the system to a heater
power cable(via the onboard relay) for example and allows the user to specify
an exact temperature which allows extremely customised temperature control in
contrast to adjusting the temperature with a control knob as with most generic
heaters. This allows for a room temperature to be decently maintained.

<h3>Connection Guide</h3>

Connect the amplifier to the analog input pin of the microcontroller
Use the output signal to manipulate a connected relay
The use of a control algoriithm regulates the temperature control
