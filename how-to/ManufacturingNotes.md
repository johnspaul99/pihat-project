<h1>Manufacturing How-to Guide</h1>

The following steps will walk you through the steps to manufacture a version of the uPiHat:

<h3>Step 1</h3>
Open by the PCB design for the main circuit. The file can be found here: https://gitlab.com/johnspaul99/pihat-project/-/tree/master/PCB
<h3>Step 2</h3>
In PCB View, go to Files -> Plot -> Gerber Files

<h3>Step 3</h3>
Use the default selections and click 'Plot'. This will generate the gerber files needed for manufacturing.

<h3>Step 4</h3>
In PCB View, go to Files -> Fabrication Outputs -> Drill File

<h3>Step 5</h3>
Leave the default selections and click 'Generate Drill Fil'e. This will generate the drill files needed for manufacturing.

<h3>Step 6</h3>
In PCB View, go to Files -> Fabrication Outputs -> BOM File

<h3>Step 7</h3>
Press 'Save'. This will generate the bill of materials files needed for manufacturing.

The bill of materials file can also be found here: https://gitlab.com/johnspaul99/pihat-project/-/tree/master/PCB

<h3>Step 8</h3>
In PCB View, go to Files -> Fabrication Outputs -> Footprint Position Files

<h3>Step 9</h3>
Leave the default selections and click 'Generate Position File'. This will generate the footprint position files needed for manufacturing.

<h3>Step 10</h3>
Send all of these files to your PCB manufacturer of choice for manufacturing.
