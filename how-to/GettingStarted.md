# This document serves as guide on how to operate the Temperature controller microPiHat

1. CONNECTING THE PI_HAT TO THE RASPBERRY_PI
	- Plug the microPihat firmly and securely into the raspberryPi headers so that the body of the PiHat aligns with the RaspberryPi.

2. CONNECTING THE DESIRED DEVICE FOR CONTROL TO THE PI_HAT
	- Choose either the positive or negative wire of your device cable to operate on. Ensure that you only work with that wire.
	- Split the desired (positive OR negative) wire into two parts. Your split wire should have exposed copper (one on each end of the split).
	- Insert each exposed copper side into the Relay widget ports (one copper exposed end per port).
	- Screw down the relay ports firmly onto the exposed copper ends of the wire.

3. PROVIDE POWER TO THE MICRO_PI_HAT
	- Plug the correct cylindrical socket charger into the drum port on the MicroPiHat.
	- Your device should now be powered. 

4. INDICATION

	- Once you have set a desired temperature for your device - the LED's will indicate the status of the temperature.
		- RED LED ON = The device temperature is currently above the desired temperature.
		- GREEN LED ON = The device temperature is currently at the desired temperature.
   		- BLUE LED ON = the device temperature is currently below the desired temperature.
   
